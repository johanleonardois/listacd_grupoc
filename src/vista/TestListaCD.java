/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;
import colecciones.ListaCD;
import java.util.Iterator;
/**
 *
 * @author estudiante
 */
public class TestListaCD {
    
    
    public static void main(String[] args) {
        
     ListaCD<String> nombres=new ListaCD();
     nombres.insertarInicio("peña");
     nombres.insertarInicio("boris");
     nombres.insertarInicio("alejandro");
     nombres.insertarFinal("leydi");
     System.out.println(nombres);
     
     for(String dato:nombres)
            System.out.println(dato);
     
     
//     //forma 2 de usar iterador:
//     Iterator<String> it=nombres.iterator();
//     while(it.hasNext())
//            System.out.println(it.next().toString());
//     
//     
//     
//     ListaCD<Integer> numeros=new ListaCD();
//     int n=1000000;
//     
//     long tiempo=System.currentTimeMillis();
//     for(int i=0;i<n;i++)
//        numeros.insertarFinal(i);
//     
//     tiempo=System.currentTimeMillis()-tiempo;
//     tiempo=tiempo/1000;
//        System.out.println("Se demoro en insertando:"+tiempo+"segundos");
//     //De la manera menos menos menos óptima:
//     //Estamos simulando un vector   
//     tiempo=System.currentTimeMillis();
//     //variable mentirosa:
//     long c=0;
////      for(int i=0;i<numeros.getTamanio();i++)  
////            //System.out.println(numeros.get(i));
////            c+=numeros.get(i);
//        
//        for(int dato:numeros)
//              c+=dato;
//
//      tiempo=System.currentTimeMillis()-tiempo;
//     tiempo=tiempo/1000;
//        System.out.println("Se demoro en iterator:"+tiempo+"segundos");
    
    
     ListaCD<Integer> numeros = new ListaCD();
        numeros.insertarFinal(1);
        numeros.insertarFinal(2);
        numeros.insertarFinal(3);
        numeros.insertarFinal(4);

        ListaCD<Integer> numeros2 = new ListaCD();
        numeros2.insertarFinal(20);
        numeros2.insertarFinal(15);
        numeros2.insertarFinal(3);

        System.out.println(numeros.concatLista(0, numeros2));
        
        //numeros.removerTodos(15);
        //numeros.removerUnoFinal(15);
        System.out.println(numeros);
        
        System.out.println(numeros.contieneRepetidos());
    }
}
