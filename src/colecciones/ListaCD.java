/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colecciones;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author estudiante
 * @param <T>
 */
public class ListaCD<T> implements Iterable<T> {

    private NodoD<T> cabeza;
    private int tamanio;

    public ListaCD() {

        this.cabeza = new NodoD();
        this.cabeza.setSig(cabeza);
        this.cabeza.setAnt(cabeza);
        //sobra:
        this.cabeza.setInfo(null);

    }

    public void insertarInicio(T info) {
        NodoD<T> nuevo = new NodoD<T>(info, this.cabeza.getSig(), this.cabeza);
        //redireccionar
        this.cabeza.getSig().setAnt(nuevo);
        this.cabeza.setSig(nuevo);

        this.tamanio++;
    }

    //O(1)
    public void insertarFinal(T info) {
        NodoD<T> nuevo = new NodoD<T>(info, this.cabeza, this.cabeza.getAnt());
        this.cabeza.setAnt(nuevo);
        nuevo.getAnt().setSig(nuevo);
        this.tamanio++;
    }

    public String toString() {
        String msg = "";
        for (NodoD<T> x = this.cabeza.getSig(); x != this.cabeza; x = x.getSig()) {
            msg += x.getInfo() + "<->";
        }
        return msg;
    }

    public boolean esVacio() {

        return (this.cabeza == this.cabeza.getSig());
        //return (this.cabeza==this.cabeza.getAnt());
        //return this.tamanio==0;
    }

    public int getTamanio() {
        return tamanio;
    }

    public T get(int i) {
        try {
            return this.getPos(i).getInfo();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }

    }

    public void set(int i, T info) {
        try {
            this.getPos(i).setInfo(info);
        } catch (Exception e) {
            System.err.println(e.getMessage());

        }
    }

    public NodoD<T> getPos(int i) throws Exception {
        if (this.esVacio() || i < 0 || i >= this.tamanio) {
            throw new Exception("El índice esta fuera de rango de la lista circular doble");
        }

        NodoD<T> x = this.cabeza.getSig();

        while (i > 0) {
            x = x.getSig();
            i--;
        }
        return x;
    }

    public ListaCD<T> concatLista(int i, ListaCD<T> l2) {
        if (this.esVacio()) {
            this.cabeza = l2.cabeza;
            return this;
        } else if (l2.esVacio()) {
            return this;
        }
        try {
            NodoD<T> aux = getPos(i);
            aux.getSig().setAnt(l2.cabeza.getAnt());
            l2.cabeza.getAnt().setSig(aux.getSig());
            l2.cabeza.setAnt(null);
            aux.setSig(l2.cabeza.getSig());
            l2.cabeza.getSig().setAnt(aux);
            l2.cabeza.setSig(null);
            return this;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }

    }

    public void removerTodos(T info) {
        NodoD<T> aux = this.cabeza;
        for (T nodo : this) {
            if (aux.getInfo() == info) {
                NodoD<T> aux2;
                aux2 = aux.getSig();
                aux.getAnt().setSig(aux.getSig());
                aux.getSig().setAnt(aux.getAnt());
                aux.setAnt(null);
                aux.setSig(null);
                aux = aux2;
            }
            aux = aux.getSig();
        }
    }

    public void removerUnoFinal(T info) {
        NodoD<T> aux = this.cabeza.getAnt();
        while (aux != null) {
            if (aux.getInfo() == info) {
                aux.getAnt().setSig(aux.getSig());
                aux.getSig().setAnt(aux.getAnt());
                aux.setAnt(null);
                aux.setSig(null);
                return;
            }
            aux = aux.getAnt();
        }
    }

    public Boolean contieneRepetidos() {
        NodoD<T> aux = this.cabeza.getSig();
        while (aux.getInfo() != null) {
            if (numVecesRepetido(aux)>0) {
                return true;
            }
            aux = aux.getSig();
        }
        return false;
    }

    public Integer numVecesRepetido(NodoD<T> node) {
        int contador = 0;
        for (T nodo : this) {
            if (node.getInfo() == nodo) {
                contador++;
            }
        }
        return contador-1;
    }

    public void removerRepetidos() {
        
    }

    @Override
    public Iterator<T> iterator() {
        return new IteratorListaCD(this.cabeza);
    }

}
